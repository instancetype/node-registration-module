/**
 * Created by instancetype on 8/6/14.
 */
const
  events = require('events')
, util = require('util')
, Registration = require('./lib/registration')
, Authentication = require('./lib/authentication')
, db = require('secondthought')
, assert = require('assert')

function Membership(dbName) {
  var self = this
  events.EventEmitter.call(self)

  self.findUserByToken = function(token, next) {
    db.connect({ db : dbName }, function(err, db) {
      assert.ok(err === null, err)
      db.users.first({ authenticationToken : token }, next)
    })
  }

  self.authenticate = function(email, password, next) {
    db.connect(
      { db : dbName }
    , function(err, db) {
        var auth = new Authentication(db)

        auth.on('authenticated', function(authResult) {
          self.emit('authenticated', authResult)
        })

        auth.on('not-authenticated', function(authResult) {
          self.emit('not-authenticated', authResult)
        })

        auth.authenticate(
          { email    : email
          , password : password
          }
        , next
        )
      }
    )
  }

  self.register = function(email, password, confirm, next) {
    db.connect(
      { db : dbName }
      , function(err, db) {
        var reg = new Registration(db)

        reg.on('registered', function(regResult) {
          self.emit('registered', regResult)
        })

        reg.on('not-registered', function(regResult) {
          self.emit('not-registered', regResult)
        })

        reg.applyForMembership(
          { email    : email
          , password : password
          , confirm  : confirm
          }
          , next
        )
      }
    )
  }
  return self
}

util.inherits(Membership, events.EventEmitter)
module.exports = Membership

