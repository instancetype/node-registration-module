/**
 * Created by instancetype on 8/4/14.
 */
const assert = require('assert')

const Log = function(args) {
  assert.ok(
      args.subject &&
      args.entry &&
      args.userId
  ,  'Needs subject, entry, and user ID'
  )

  var log = {}

  log.subject = args.subject
  log.entry = args.entry
  log.userId = args.userId
  log.createdAt = new Date()

  return log
}

module.exports = Log