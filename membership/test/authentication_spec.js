/**
 * Created by instancetype on 8/6/14.
 */
const
  Registration = require('../lib/registration')
, Auth = require('../lib/authentication')
, db = require('secondthought')
, assert = require('assert')
, should = require('should')

describe('Authentication', function () {
  var reg = {}
    , auth = {}
    , aUser = { email    : 'test@example.com'
              , password : 'yeah'
              , confirm  : 'yeah'
              }
  before(function(done) {
    db.connect({ db : 'membership' }, function(err, db) {
      reg = new Registration(db)
      auth = new Auth(db)
      db.users.destroyAll(function(err, result) {
        reg.applyForMembership(
          aUser
          , function(err, regResult) {
            assert.ok(regResult.success)
            done()
          }
        )
      })
    })
  })

  describe('a valid login', function () {
    var authResult = {}
    var anotherUser = { email : 'test@example.com', password : 'yeah' }
    before(function(done) {
      auth.authenticate(
        anotherUser
      , function(err, result) {
          assert.ok(err === null, err)
          authResult = result
          done()
        }
      )
    })


    it('is successful', function() {
      authResult.success.should.equal(true)
    })
    it('returns a user', function() {
      authResult.user.should.be.defined
    })
    it('creates a log entry', function() {
      should.exist(authResult.log)
    })
    it('updates the user stats', function() {
      authResult.user.signInCount.should.equal(2)
    })
    it('updates the signon dates', function() {
      should.exist(authResult.user.currentLoginAt)
      should.exist(authResult.user.lastLoginAt)
      authResult.user.currentLoginAt.should.not.equal(authResult.user.lastLoginAt)
    })
  })

  describe('empty email', function () {
    var authResult = {}
    var aUser = { email : null, password : 'yeah' }
    before(function(done) {
      auth.authenticate(
        aUser
        , function(err, result) {
          assert.ok(err === null, err)
          authResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      authResult.success.should.equal(false)
    })
    it('returns a message saying "Invalid login"', function() {
      authResult.message.should.equal('Invalid email or password')
    })
  })

  describe('empty password', function () {
    var authResult = {}
    var aUser = { email : 'test@example.com', password : null }
    before(function(done) {
      auth.authenticate(
        aUser
        , function(err, result) {
          assert.ok(err === null, err)
          authResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      authResult.success.should.equal(false)
    })
    it('returns a message saying "Invalid login"', function() {
      authResult.message.should.equal('Invalid email or password')
    })
  })

  describe('password doesn\'t match', function () {
    var authResult = {}
    var aUser = { email : 'test@example.com', password : 'nope' }
    before(function(done) {
      auth.authenticate(
        aUser
        , function(err, result) {
          assert.ok(err === null, err)
          authResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      authResult.success.should.equal(false)
    })
    it('returns a message saying "Invalid login"', function() {
      authResult.message.should.equal('Password doesn\'t match')
    })
  })

  describe('email not found', function () {
    var authResult = {}
    var aUser = { email : 'nope@example.com', password : 'yeah' }
    before(function(done) {
      auth.authenticate(
        aUser
        , function(err, result) {
          assert.ok(err === null, err)
          authResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      authResult.success.should.equal(false)
    })
    it('returns a message saying "Invalid login"', function() {
      authResult.message.should.equal('User not found')
    })
  })
})