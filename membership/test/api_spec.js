/**
 * Created by instancetype on 8/6/14.
 */
const
  db = require('secondthought')
, Membership = require('../index')
, assert = require('assert')

describe('Main API', function () {
  var memb = {}
  before(function(done) {
    memb = new Membership('membership')
    db.connect({ db : 'membership' }, function(err, db) {
      db.users.destroyAll(function(err, result) {
        done()
      })
    })
  })

  describe('authentication', function () {
    var newUser = {}
    before(function(done) {
      memb.register('dude@example.com', 'pass', 'pass', function(err, result) {
        newUser = result.user
        assert.ok(result.success, 'Cannot register')
        done()
      })
    })

    it('authenticates', function(done) {
      memb.authenticate('dude@example.com', 'pass', function(err, result) {
        result.success.should.equal(true)
        done()
      })
    })
    
    it('gets user by token', function(done) {
      memb.findUserByToken(newUser.authenticationToken, function(err, result) {
        result.should.be.defined
        done()
      })
    })
  })
})

