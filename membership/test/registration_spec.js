/**
 * Created by instancetype on 8/5/14.
 */
const
  Registration = require('../lib/registration')
, db = require('secondthought')

describe('Registration', function() {
  var reg = {}
  before(function(done) {
    db.connect({ db : 'membership' }, function(err, db) {
      reg = new Registration(db)
      done()
    })
  })

  // Happy Path
  describe('a valid application', function () {
    var regResult = {}
    before(function(done) {
      db.users.destroyAll(function(err, result) {
        reg.applyForMembership(
          { email    : 'dude@example.com'
          , password : 'yeah'
          , confirm  : 'yeah'
          }
          , function(err, result) {
            regResult = result
            done()
          }
        )
      })
    })

    it('is successful', function() {
      regResult.success.should.equal(true)
    })
    it('creates a user', function() {
      regResult.user.should.be.defined
    })
    it('creates a log entry', function() {
      regResult.log.should.be.defined
    })
    it('sets the user\'s status to approved', function() {
      regResult.user.status.should.equal('approved')
    })
    it('offers a welcome message', function() {
      regResult.message.should.equal('Welcome!')
    })
    it('increments the signInCount', function() {
      regResult.user.signInCount.should.equal(1)
    })
  })

  describe('an empty or null email', function () {
    var regResult = {}
    before(function(done) {
      reg.applyForMembership(
        { email    : null
        , password : 'yeah'
        , confirm  : 'yeah'
        }
        , function(err, result) {
          regResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      regResult.success.should.equal(false)
    })
    it('tells the user an email is required', function() {
      regResult.message.should.equal('Email and password are required')
    })
  })

  describe('an empty or null password', function () {
    var regResult = {}
    before(function(done) {
      reg.applyForMembership(
        { email    : 'dude@example.com'
        , password :  null
        , confirm  : 'yeah'
        }
        , function(err, result) {
          regResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      regResult.success.should.equal(false)
    })
    it('tells the user a password is required', function() {
      regResult.message.should.equal('Email and password are required')
    })
  })

  describe('password and confirm mismatch', function () {
    var regResult = {}
    before(function(done) {
      reg.applyForMembership(
        { email    : 'dude@example.com'
        , password : 'yeah'
        , confirm  : 'nope'
        }
        , function(err, result) {
          regResult = result
          done()
        }
      )
    })
    it('is not successful', function() {
      regResult.success.should.equal(false)
    })
    it('tells the user the password does not match', function() {
      regResult.message.should.equal('Passwords do not match')
    })
  })

  describe('email already exists', function () {
    var regResult = {}
    before(function(done) {
      var newUser = { email    : 'dude@example.com'
                    , password : 'yeah'
                    , confirm  : 'yeah'
                    }
      db.users.destroyAll(function(err, deleted) {
        reg.applyForMembership(newUser, function(err, result) {
          reg.applyForMembership(newUser, function(err, nextResult) {
            regResult = nextResult
            done()
          })
        })
      })
    })
    it('is not successful', function() {
      regResult.success.should.equal(false)
    })
    it('tells the user the email already exists', function() {
      regResult.message.should.equal('This email already exists')
    })
  })
})