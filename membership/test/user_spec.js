/**
 * Created by instancetype on 8/4/14.
 */
const
  should = require('should')
, User = require('../models/user')

describe('User', function() {

  describe('defaults', function() {
    var user = {}

    before(function() {
      user = new User({ email : "dude@example.com" })
    })

    it('email is dude@example.com', function() {
      user.email.should.equal('dude@example.com')
    })
    it('has an authentication token', function() {
      user.authenticationToken.should.be.defined
    })
    it('has a pending status', function() {
      user.status.should.equal('pending')
    })
    it('has a created date', function() {
      user.createdAt.should.be.defined
    })
    it('has a signInCount of 0', function() {
      user.signInCount.should.equal(0)
    })
    it('has lastLogin', function() {
      user.lastLoginAt.should.be.defined
    })
    it('has currentLogin', function() {
      user.currentLoginAt.should.be.defined
    })
  })
})