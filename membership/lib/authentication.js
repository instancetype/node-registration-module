/**
 * Created by instancetype on 8/6/14.
 */
const
  events = require('events')
, util = require('util')
, assert = require('assert')
, bcrypt = require('bcrypt-nodejs')
, User = require('../models/user')
, Log = require('../models/log')

const AuthResult = function(creds) {

  const result = { creds   : creds
                 , success : false
                 , message : null
                 , user    : null
                 , log     : null
                 }
  return result
}


const Authentication = function(db) {
  var self = this
    , continueWith = null
  events.EventEmitter.call(self)

  // validate credentials
  var validateCredentials = function(authResult) {
    if (authResult.creds.email && authResult.creds.password) {
      self.emit('creds-ok', authResult)
    }
    else {
      authResult.message = 'Invalid email or password'
      self.emit('invalid', authResult)
    }
  }
  // find user
  var findUser = function(authResult) {
    db.users.first(
      { email : authResult.creds.email }
    , function(err, found) {
        assert.ok(err === null, err)
        if (found) {
          authResult.user = new User(found)
          self.emit('user-found', authResult)
        }
        else {
          authResult.message = 'User not found'
          self.emit('invalid', authResult)
        }
      }
    )
  }
  // compare password
  var comparePassword = function(authResult) {
    bcrypt.compare(
      authResult.creds.password
    , authResult.user.hashedPassword
    , function(err, match) {
        if (match) {
          self.emit('password-accepted', authResult)
        }
        else {
          authResult.message = "Password doesn't match"
          self.emit('invalid', authResult)
        }
      }
    )
  }
  // bump stats
  var updateUserStats = function(authResult) {
    var user = authResult.user
    user.signInCount += 1
    user.lastLoginAt = user.currentLoginAt
    user.currentLoginAt = new Date()

    var updates = { signInCount    : user.signInCount
                  , lastLoginAt    : user.lastLoginAt
                  , currentLoginAt : user.currentLoginAt
                  }

    db.users.updateOnly(
      updates
    , user.id
    , function(err, updates) {
        assert.ok(err === null, err)
        self.emit('stats-updated', authResult)
      }
    )
  }
  // create log entry
  var createLog = function(authResult) {
    var log = new Log(
      { subject : 'Authentication'
      , userId  : authResult.user.id
      , entry   : 'Successfully logged in'
      }
    )
    db.logs.save(log, function(err, newLog) {
      authResult.log = newLog
      self.emit('log-created', authResult)
    })
  }

  var authOk = function(authResult) {
    authResult.success = true
    authResult.message = 'Welcome!'
    self.emit('authenticated', authResult)
    self.emit('completed', authResult)

    if (continueWith) continueWith(null, authResult)
  }

  var authNotOk = function(authResult) {
    authResult.success = false
    self.emit('not-authenticated', authResult)
    self.emit('completed', authResult)

    if (continueWith) continueWith(null, authResult)
  }

  // happy path
  self.on('login-received', validateCredentials)
  self.on('creds-ok', findUser)
  self.on('user-found', comparePassword)
  self.on('password-accepted', updateUserStats)
  self.on('stats-updated', createLog)
  self.on('log-created', authOk)

  // sad path
  self.on('invalid', authNotOk)

  self.authenticate = function(creds, next) {
    continueWith = next
    var authResult = new AuthResult(creds)
    self.emit('login-received', authResult)
  }
  return self
}

util.inherits(Authentication, events.EventEmitter)

module.exports = Authentication