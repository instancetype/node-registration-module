/**
 * Created by instancetype on 8/4/14.
 */
exports.randomString = function(length) {
  var
    stringLength = length || 12
  , chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
  , result = ''
  , i

  for (i = 0; i < stringLength; ++i) {
    var rnum = Math.floor(Math.random() * chars.length)
    result += chars.substring(rnum, rnum+1)
  }
  return result
}